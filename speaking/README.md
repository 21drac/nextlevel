# Aprendiendo y mejorando nuestro speaking
*******
Esta recopilacion de material tiene como objetivo poder rendir el examen internacional TOEFL el cual mide nuestra capacidad de utilizar y de entender el inglés a nivel universitario. Además, evalúa cómo combina sus habilidades auditivas, orales, de lectura y de escritura para desarrollar tareas académicas.
*******
## Temario | Silabo

1. **¿Quién rinde el examen TOEFL?**    

2. **¿Quién acepta las calificaciones del examen TOEFL?**

3. **¿Qué recursos pueden ayudarme a prepararme para el examen TOEFL iBT?**
- [ ] Libros oficiales del test 
- [ ] Simulador Online 
- [ ] TOEFL iBT® Test Prep Planner

*******
## Material Oficial
- [TOEFL iBT® Test Prep Planner](https://www.ets.org/es/toefl/ibt/prepare/test_prep_planner)
- [Simulador Online](https://www.ets.org/s/toefl/free-practice/start.html)
- [Libro practico de Test](https://www.ets.org/s/toefl/pdf/free_practice_test_large_print.pdf)

## Material Recomendado
- [Canal de YouTube: Amigos Ingleses](https://www.youtube.com/user/AmigosIngleses/videos)
- [App: Clases Online Free/Paga](https://learn.abaenglish.com/es/abaenglish-influencers/)
- [App: Droop](https://languagedrops.com/)
- [Canal de Youtube: Karen Munoz](https://www.youtube.com/watch?v=2vq9yNIF2EA&list=PLu62H3EFBS2CdE5-kSn8jlqbbMalDCHRm)
