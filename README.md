# Next Level

### Es un programa de educación híbrida. 
*********************
<img src="https://i.postimg.cc/fRhg3Zc1/nextlevel.jpg" width="525"/>

**Student Network** busca generar soluciones y este programa centrado en la educación es una solución que busca potenciar el auto aprendizaje em este repositorio encontraras materiales de los entrenamientos del programa que se realizará en los meses de junio a noviembre con el apoyo de las diversas comunidades de software del país.

A continuación encontrará un conjunto de silabus que muestran las rutas que puede tomar y las habilidades que desea adoptar para evolucionar tus conocimientos. Los temas que se encuentran estan desde; 
* python
* javascrip
* latex
* agile
* aid
* cloud
* db
* emotional
* finance
* interactive
* java
* management
* parallel
* pitch
* r
* relational
* skills
* speaking
* spiritual

y estos pueden ir aumentando para ayudar a la comunidad.

## Propósito de estas hojas de ruta
El propósito de estas hojas de ruta es darte una idea sobre el paisaje y guiarte si estás confundido sobre qué aprender a continuación y no animarte a elegir lo que está de moda y moda. Usted debe crecer con un poco de comprensión de por qué una herramienta sería más adecuado para algunos casos que el otro y recordar la cadena y la moda nunca significa más adecuado para el trabajo.

## Nota para principiantes
Estas hojas de ruta cubren todo lo que hay que aprender para los caminos que se enumeran. No te sientas abrumado, no necesitas aprenderlo todo al principio si estás empezando. Estamos trabajando en las versiones para principiantes de estos y lo lanzaremos poco después de que hayamos terminado con la versión 2020 de las hojas de ruta de JavaScrip, Python y LaTeX.

Si cree que alguna de las hojas de ruta se puede mejorar, no dude en discutirla en los temas. Además, seguiré mejorando esto, por lo que es posible que desee ver este repositorio o suscribirse en studentnetwork.gitlab.io para volver a visitar.

## Contribución
Eche un vistazo a los documentos de contribución para saber cómo actualizar cualquiera de las hojas de ruta

> Abrir solicitud de extracción con mejoras

> Discutir ideas en temas

> Difundir la palabra

> Comuníquese con cualquier comentario 