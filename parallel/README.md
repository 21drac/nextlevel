# Aprendiendo sobre Computación en Paralelo
*******
La computación paralela es una forma de cómputo en la que muchas instrucciones se ejecutan simultáneamente, operando sobre el principio de que problemas grandes, a menudo se pueden dividir en unos más pequeños, que luego son resueltos simultáneamente.
*******
## Temario | Silabo

1. **Computación basada en FPGA**  
- 1.1. Introduccion al diseño electronico con FPGA y VHDL
- [ ] Tecnlogias de implementacion FPGA, ASIC
- [ ] Lenguajes de descripcion de Hardware VHDL y Verilog
- 1.2. Elementos basicos de VHDL
- [ ] Estructura de un diseño VHDL entidades y arquitectura
- [ ] Señales y variables
- 1.3. Tipos de datos en VHDL 
- [ ] Tipos de datos VHDL 
- [ ] Tipos de dato Standard logic/ standard logic vector
- [ ] Tipos de dato Unsigned/signed
- [ ] Tipos de dato Integer y Boolean
- [ ] Arrays y estructuras
- [ ] Ejemplos (inicializar valores)
- [ ] Primer ejemplo en VHDL: Sumador n bits
- 1.4. Estructuras del lenguaje VHDL 
- [ ] Estilos descriptivos flujo de datos, estructural y de comportamiento 
- [ ] Estilo descriptivosflujo de datos
- [ ] Estilos descriptivos estructural
- [ ] Estilos descriptivos de comportamiento, sentencias condicionales
- [ ] Estilos descriptivos de comportamiento, procesos 
- [ ] Estilos descriptivos de comportamiento, bucles
- [ ] Diferencia de implementacion entre Lf y Case
- [ ] Descripcion estructural:Sumadores basicos
- [ ] Descripcion comportamiento:Sumadores
- 1.5. Diseñando circuitos con VHDL 
- [ ] Sistemas combinacionales y secuenciales 
- [ ] Sistema combinacional: ALU
- [ ] Sistema secuencial: Contador ascendente
- [ ] Maquinas de estado, Mealy y Moore
- [ ] Memorias
- 1.6. Test de circuitos diseñados en VHDL 
- [ ] Esjemplo Maquina de estado
- [ ] Testbenches avanzados
- [ ] Tesbenches avanzados
- [ ] Ejemplos de tests de los apartados anteriores en Modelsim
- 1.7. Herramientas de diseño 
- [ ] Instalacio Simulador Modelsim 
- [ ] Xilinx
- [ ] Placa de prototipado
- [ ] Restriccioones
- [ ] Altera   

2. **Computación basada en Fotonica Integrada**
- 2.1. Introduccion al electromagnetismo Aplicado
- [ ] Fotonica Integrada y Fotonica de Silicio
- [ ] Lumerical
- [ ] Leyes de Maxwell 
- 2.2. Educacion de onda electromagnetica
- [ ] Ecuacion de onda electromagnetica de la forma helmholtz
- [ ] Condiciones de frontera
- [ ] Lumerical Knowledgebase
- 2.3. Analisis modal en guias de onda dielectricas 
- [ ] Guias de onda dielectricas 
- [ ] Guias de onda slab: analisis modal
- [ ] Modo transversal electronico y transversal magnetico
- [ ] Lumerial - FDE solver
- [ ] Simulacion de una guia de onda canal
- 2.4. Dispositivos fotonicos pasivos 
- [ ] Spliter de potencia - Apartir de interferometria modal 
- [ ] Interferometro de Mach-Zehnder
- [ ] Teoria de modos acoplados
- [ ] Cristales fotonicos

3. **Computación basada en Cuántica**
- 3.1. Criptografia Cuantica
- [ ] Fotones
- [ ] Polarizacion de fotones
- [ ] Teorema de no clonacion
- [ ] Codificacion con XOR
- [ ] Cifrado con secretos
- [ ] Codificacion de datos en polarizacion de fotones
- [ ] Hacer el protocolo seguro
- [ ] Intercambio de angulos de polarizacion
- [ ] Protocolo BB84 
- 3.2. Fundamento en numeros complejos, probabilidad, algebra linealy logica 
- [ ] Fundamentos de Matematica 
- [ ] Probabilidad
- [ ] Numeros complejos 1 
- [ ] Numeros complejos 2
- [ ] Numeros complejos 3 
- [ ] Algebra Matricial
- [ ] Multiplicacion de matrixes 1 
- [ ] Multiplicacion de matrixes 2
- [ ] Matrices de identidad 
- [ ] Matrices de columnas
- [ ] XxX matrices 
- [ ] Circuitos logicos
- 3.3. Desarrollo de un modelo matematico para fisica cuantica 
- [ ] Modelando Fisica con Matematicas 
- [ ] Probabilidades sustractivas a traves de numeros complejos
- [ ] Modelado de superposion a traves de matrices
- [ ] Descripcion general del modelo matematico
- 3.4. Fisica Cuantica de los Estados de Spin
- [ ] Estados de giro
- [ ] Base
- [ ] Representacion matricial de columnas del estado cuantico
- [ ] Vector de estado
- [ ] Spin 1
- [ ] Spin 2
- [ ] Spin 3 
- 3.5. Modelado de estados de giro cuantico de matematicas 
- [ ] Analisis 1 
- [ ] Analisis 2 
- [ ] Analisis 3
- [ ] Dirac Sujetador-Ket Notacion 2 
- [ ] Analisis 4
- [ ] Analisis 5
- [ ] Comportamiento aleatorio
- 3.6. Transformaciones de estado reversibles e irreversibles
- [ ] Transformaciones irreversibles: medicion
- [ ] Transformaciones de estado reversibles
- 3.7. Sistemas Multi-Qubit
- [ ] Analizando Sistemas Multi-Qubit 
- 3.8. Entrelazamiento 
- [ ] Entrelazamiento 
- [ ] Mas fisica cuantica
- 3.9. Modelo de computacion cuantica 
- [ ] Circuitos cuanticos 
- [ ] Fanout
- [ ] Sin computacion
- [ ] Puertas reversibles
- [ ] Quantum NO 
- [ ] Otras puertas Qubit individuales
- [ ] CNOT Gate
- [ ] CCNOT: Puerta de Toffoli
- [ ] Puerta universal 
- [ ] Fredkin Gate
- [ ] Efectos de la superposicion y el enredo en las puertas cuanticas

*******
## Libros Recomendados
- [ ] Sistemas Embebidos FPGA - Ricardo Cassials
- [ ] FPGAs for Dummies - Intel

## Material 
- [FPGAs for Dummies](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/misc/fpgas_for_dummies_ebook.pdf)
- [Priorizando lineas de investigacion](http://www.fotonica21.org/rs/1041/d112d6ad-54ec-438b-9358-4483f9e98868/016/filename/fotonica21-libro-priorizacion-lineas-idi-final-1p.pdf)
