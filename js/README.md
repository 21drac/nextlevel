# Aprendiendo JavaScript
*******
JavaScript le permite agregar funciones interactivas a sus sitios web, incluido contenido actualizado dinámicamente, multimedia controlada, imágenes animadas y mucho más.En los ultimos años a evolucionado hasta convertise algo de proposito general y esta presente en multitud ser aplicaciones/servicios esta recopilacion tiene como objetivo comprender los conceptos básicos del lenguaje. 
*******
## Temario | Silabo

1. **Introducción a JavaScript**

- [ ] JavaScript, HTML y CSS
- [ ] Descripción general de JavaScript
- [ ] Tu primera página HTML / CSS / JS
- [ ] Variables, valores, funciones, operadores y expresiones.
- [ ] Ejemplos simples de JavaScript para jugar

2. **Agregar interactividad a HTML**

- [ ] Declaraciones condicionales, bucles y operadores lógicos.
- [ ] Funciones y devoluciones de llamada
- [ ] Manejo de eventos
- [ ] La API DOM
- [ ] Escribamos un pequeño juego

3. **Jugar con HTML5**

- [ ] Matrices e iteradores de API
- [ ] Multimedia HTML5 y API de JavaScript
- [ ] Mostrar un mapa con la API de geolocalización
- [ ] Reproducción de muestras de sonido y música.

4. **Estructuración de datos**

- [ ] Objetos, propiedades y métodos.
- [ ] Crear múltiples objetos
- [ ] Organizando el código en archivos separados
- [ ] Mejorando el juego con clases de ES6

5. **Trabajando con formularios**

- [ ] Objetos JavaScript incorporados
- [ ] Tablas HTML5, formularios y campos de entrada.
- [ ] La notación JSON
- [ ] Creemos una pequeña aplicación
*******
## Materiales 
- [Curso gratis EDX & W3C & Universidad de la Costa Azul](https://www.edx.org/course/javascript-introduction)
- [Libro gratis de autor Independiente & Frontend Masters ](https://github.com/getify/You-Dont-Know-JS)
