# Aprendiendo sobre los servicios Cloud más populares
*******
Los servicios cloud conocida también como servicios en la nube, informática en la nube, nube de cómputo, nube de conceptos o simplemente «la nube», es un paradigma que permite ofrecer servicios de computación a través de una red, que usualmente es Internet.
*******
## Temario | Silabo

1. **Microsoft Azure**   
- 1.1. Implemente soluciones que usen máquinas virtuales
- [ ] Aprovisionar máquinas virtuales
- [ ] Crear plantillas ARM
- [ ] Configurar Azure Disk Encryption para máquinas virtuales 
- 1.2. Implemente trabajos por lotes mediante Azure Batch Services 
- [ ] Descripción general de Azure Batch 
- [ ] Ejecute un trabajo por lotes mediante la CLI de Azure y el Portal de Azure
- [ ] Ejecute trabajos por lotes utilizando código 
- [ ] Administre trabajos por lotes utilizando la API de servicio por lotes
- 1.3. Crear soluciones en contenedores 
- [ ] Crear un clúster de Azure Managed Kubernetes Service (AKS)
- [ ] Crear imágenes de contenedor para soluciones
- [ ] Publicar una imagen en el Registro de contenedor de Azure
- [ ] Ejecute contenedores con Azure Container Instance o AKS

2. **Google Cloud**
- 2.1. Presentación de Google Cloud Platform
- [ ] VPS 
- 2.2. Almacenamiento en la nube 
- [ ] Cloud Storage 
- [ ] Cloud SQL
- [ ] Cloud Spanner 
- [ ] Cloud Datastore 
- [ ] Google Bigtable
- 2.3. Contenedores en la Nube 
- [ ] Kubernetes Engine
- [ ] App Engine en la plataforma se llama como servicio ("PaaS")

3. **AWS**
- 3.1. IAM - Gestión de acceso e identidad
- 3.2. S3 - Almacenamiento de datos seguro en la nube 
- 3.3. CloudFront - Distribuir datos globalmente 
- 3.4. Storage Gateway - Nube híbrida
- 3.5. Snowball - Migración de datos 
- 3.6. EC2 - Capacidad informática en la nube
- 3.7. CloudWatch - Monitorización de los servicios AWS
- 3.8. EFS - Almacenamiento de archivos flexible 
- 3.9. Lambda - Ejecutar código sin servidores
- 3.10. Route 53 - Gestionar dominios en el cloud
- 3.11. Bases de datos

*******
## Material 
- [Cursos Azure](https://www.microsoft.com/es-es/learning/azure-training.aspx)
- [Cursos GCP](https://cloud.google.com/training?hl=es)
- [Cursos AWS](https://aws.amazon.com/es/training/course-descriptions/cloud-practitioner-essentials/)

## Certificaciones Oficiales 
- [Certificaciones Azure](https://www.microsoft.com/es-es/learning/certification-overview.aspx)
- [Certificaciones GCP](https://cloud.google.com/certification?hl=es)
- [Certificaciones AWS](https://aws.amazon.com/es/certification/)

## Mayor Informacón
Dudas y consultas [Link](https://www.instagram.com/ieeecomsocuch/)
