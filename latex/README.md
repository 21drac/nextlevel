# Aprendiendo LaTeX
*******
LaTeX, un sistema de preparación de documentos, es ampliamente utilizado para la publicación en muchos campos científicos como matemáticas, estadística, informática, ingeniería, química, física, economía, lingüística, etc. Es un sistema potente y de código abierto que ofrece numerosas instalaciones. para automatizar la composición tipográfica del documento: es decir, estructurar el diseño de la página, enumerar y numerar automáticamente secciones, tablas, figuras, generar una tabla de contenido, gestionar referencias cruzadas, citas e indexación. Lo que permite al usuario concentrarse en el contenido en lugar de la estética (la forma en que se ve). El programa de composición tipográfica TeX que usa LaTeX, fue diseñado de tal manera que cualquiera puede crear material de buena calidad con menos esfuerzo.

*******
## Temario | Silabo

1. **Introducción**

- [ ] Instalación y diferentes IDE
- [ ] Crea el primer documento usando LaTeX
- [ ] Organiza el contenido en secciones usando la clase de artículos y libros de LaTeX

2. **Diseño de páginas**

- [ ] Comienza revisando diferentes tamaños de papel
- [ ] Examina paquetes
- [ ] Formatea la página configurando márgenes
- [ ] Personalizando encabezado y pie de página
- [ ] Cambiando la orientación de la página
- [ ] Dividiendo el documento en varias columnas
- [ ] Lectura de diferentes tipos de mensajes de error

3. **Formato de contenido**

- [ ] Formatear texto (estilos, tamaño, alineación)
- [ ] Agregar colores al texto y a toda la página, y agregar viñetas y elementos numerados
- [ ] Proceso de escribir matemáticas complejas

4. **Tablas e imágenes**

- [ ] Creando tablas básicas
- [ ] Agregando bordes simples y discontinuos
- [ ] Combinando filas y columnas y manejando situaciones en las que una tabla excede el tamaño de una página
- [ ] Agregando una imagen, exploran diferentes propiedades como rotar, escalar, etc

5. **Referencia e indexación**

- [ ] Agregar referencias cruzadas (consulte secciones, tabla, imágenes)
- [ ] Agregar bibliografía (referencias)
- [ ] Crear un índice posterior

6. **Presentación con Beamer**

- [ ] Creación de diapositivas
- [ ] Agregar marcos
- [ ] Dividir la diapositiva en  varias columnas
- [ ] Agregar diferentes bloques, etc
*******
## Materiales 
- [Curso gratis EDX](https://www.edx.org/course/latex-for-students-engineers-and-scientists)
- [Libro gratis Universidad Complutense de Madrid](https://www.ucm.es/data/cont/docs/1346-2019-04-12-BaSix%20LaTeX%20ba%CC%81sico%20con%20ejercicios%20resueltos27.pdf)
- [Libro gratis Universidad de Castilla La Mancha](matematicas.earanda/wp-content/uploads/downloads/2013/10/latex.pdf)

