# Aprendiendo Base de Datos
*******
Las bases de datos son increíblemente frecuentes: subyacen a la tecnología utilizada por la mayoría de las personas todos los días, si no cada hora. Las bases de datos residen detrás de una gran fracción de sitios web; son un componente crucial de los sistemas de telecomunicaciones, sistemas bancarios, videojuegos y casi cualquier otro sistema de software o dispositivo electrónico que mantenga cierta cantidad de información persistente. Además de la persistencia, los sistemas de bases de datos proporcionan una serie de otras propiedades que los hacen excepcionalmente útiles y convenientes: confiabilidad, eficiencia, escalabilidad, control de concurrencia, abstracciones de datos e idiomas de consulta de alto nivel. Las bases de datos son tan omnipresentes e importantes es por ello que debe considerarse como algo que toda persona deberia aprender idependientemente de sus carreras industriales o de posgrado.
*******
## Temario | Silabo
*******
1. **Modelos**

- [ ] Introduccion y base de datos relacionales
- [ ] Datos XML
- [ ] Datos JASON

2. **Querying Relacional**

- [ ] Algebra relacional
- [ ] SQL

3. **Querying XML**

- [ ] XPath y XQuery
- [ ] XSLT

4. **Diseño de una base de datos**

- [ ] Teoria del diseño relacional
- [ ] Lenguaje de modelo unificado

5. **SQL Avanzado**

- [ ] Indices y transacciones
- [ ] Restricciones y disparadores
- [ ] Vistas y autorizacion
- [ ] Procesamiento analitico lineal
- [ ] Recursion en SQL

*******
###  Materiales
- [Curso gratis EDX & Stanford](https://lagunita.stanford.edu/courses/DB/2014/SelfPaced/about)
