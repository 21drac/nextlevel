# Aprendiendo sobre Inteligencia Relacional
*******
La inteligencia relacional te ayuda a mejorar habilidades como la autoconciencia, la empatía, la comprensión de la perspectiva de los demás, la precisión emocional y cognitiva, la capacidad de resonar con los demás y el manejo de las emociones. La mayoría de nosotros somos más fuertes en algunas de estas áreas que en otras pero apendiendo del tema podemos manejar mejor cada uno de los aspectos mencionados. Por que cuando tenemos relaciones de alto funcionamiento, los seres humanos florecen. ¡Tenemos más energía, más concentración, menos drama, más alegría en nuestras vidas y esa combinación dinámica significa que no podemos evitar ser productivos! Las competencias relacionales y emocionales involucran principalmente los circuitos que van desde los centros emocionales hasta los lóbulos prefrontales. El aprendizaje efectivo para la competencia emocional vuelve a cablear estos circuitos.
*******
## Libros Recomendados
- [ ] Nunca comas solo - Keith Ferrazzi y Tahl Raz
- [ ] Cómo ganar amigos e influir sobre las personas - Dale Carnegie
- [ ] Terapia Gestalt - Chantal Higy Lang, Charles Gellman
- [ ] FOCUS - Daniel Goleman
- [ ] El libro del networking - Cipri Quintas

*******
## Mayor Informacón
Dudas y consultas [Link](https://www.instagram.com/ieeecomsocuch/)
