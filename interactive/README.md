# Aprendiendo sobre Diseño Interactivo
*******
El diseño interactivo es el arreglo de gráficos, texto, videos, fotos, ilustraciones, sonidos, animación, imágenes tridimensionales, realidad virtual y otros medios en un documento interactivo. Para que una persona domine este perfil requiere un conjunto de disciplinas que engloba términos como el diseño de software, de interfaz de usuario, diseño centrado en el usuario, diseño de productos, diseño web, etc. Según Terry Winograd e Yvone Rogers, se refiere al diseño de espacios interactivos para apoyar la forma en que la gente se comunica e interactúa con su vida y quehacer diario. Es fundamental para todas las disciplinas, campos y afines preocupados por investigar y diseñar sistemas basados en computadora para el ser humano.
*******
## Temario | Silabo

1. **Diseño de Experiencia (UX)**  
- [ ] Psicología Gestalt y diseño web
- [ ] Cómo crear una cartera de UX
- [ ] Visualización de información
- [ ] Design Thinking
- [ ] Psicología del comercio electrónico
- [ ] Creatividad: métodos para diseñar mejores productos y servicios
- [ ] Patrones de diseño de interfaz de usuario
- [ ] Usabilidad

2. **Arquitectura de Información (AI)**
- [ ] Solución y la arquitectura de la información
- [ ] Diferentes roles en el proceso de diseño
- [ ] Recopilación de requisitos
- [ ] Concepción del producto
- [ ] Arquitectura de base de datos
- [ ] Requisitos técnicos
- [ ] Gestión de proyectos
- [ ] Aseguramiento de la calidad
- [ ] Gestión de la documentación
- [ ] Código y modularidad de componentes
- [ ] Diseño front-end
- [ ] Diseño
- [ ] Diseño de back-end

3. **Arquitectura (ARQ)**
- [ ] Eficiencia
- [ ] Tècnica del Card Sorting

4. **Interaccion Humana Computador (HCI)**
- [ ] El hardware y el sofware como afectan a la interaccion
- [ ] Modelos mentales de los usuarios frente al modelo de la maquina
- [ ] Campo visual
- [ ] Eye-tracking
- [ ] Factores fisicos
- [ ] Factores psicologicos de los usuarios

5. **Diseño Grafico** 
- [ ] Fundamentos visuales
- [ ] Representacion del volumen
- [ ] Mentoring
- [ ] Tipografia
- [ ] Historia de la cultura
- [ ] Diagramacion
- [ ] Fotografia
- [ ] Ilustracion
- [ ] Proceso creativo
- [ ] Comportamiento del consumidor
- [ ] Animacion digital
- [ ] Produccion grafica
- [ ] Inteligencia Social
- [ ] Direccion de arte
- [ ] Creatividad
- [ ] Lenguaje publicitario
- [ ] Campañas de publicidad

6. **Produccion de Contenido**
- [ ] Comunicacion
- [ ] Herramientas audivisuales y ediccion
- [ ] Lenguaje y estetica
- [ ] Principios basicos del guion
- [ ] Herramientas digitales
- [ ] Direccion de contenido para TV
- [ ] Iluminacion
- [ ] Narrativa audivisual
- [ ] Sonido y musicalizacion
- [ ] Diseño escenografico
- [ ] Redaccion periodistica/ Redaccion digital
- [ ] Post produccion audiovisual
- [ ] Composicion de efectos audiovisuales
- [ ] Marketing

8. **Diseño Industrial**
- [ ] Experimentacion de materiales
- [ ] Tecnologia de los materiales
- [ ] Tecnicas artesanales
- [ ] Lineas de diseño
- [ ] Antopometria y Ergonomia
- [ ] Envaces y empaques
- [ ] Dibujo tecnico de producto
- [ ] Modelado digital 3D
- [ ] Diseño sostenible
- [ ] Procesos de fabricacion
- [ ] Ecodiseño
- [ ] Ensamble digital
- [ ] Diseño y gestion
- [ ] Prototipado 3D
- [ ] Interfaces tegnologicas
- [ ] Legislacion para productos comerciales

9. **Diseño de Sonido**
- [ ] Lectura y entrenamiento auditivo
- [ ] Instrumentos
- [ ] Locucion radial
- [ ] Analisis de informacion
- [ ] Herramientas de procesado de audio
- [ ] Tecnologias de audio
- [ ] Arreglos
- [ ] Entrevistas y reportajes
- [ ] Gestion de audiencias
- [ ] Desarrollo de contenido multiplataforma

*******
## Material 
- [Sugerencia de Cursos](https://www.interaction-design.org/courses)
- [Fundamentos en Experiencia de usuario](https://www.uxerschool.com/cursos/)
- [Arquitecto perfecto de información](https://www.edx.org/es/course)
- [Teoria de Juego e Interacion Humana](http://www.editorialbonaventuriana.usb.edu.co/libros/2019/teoria-juegos-ihc/index.html)

*******
## Libros Recomendados
- [ ] Information Architecture for the World Wide Web - Louis Rosenfeld y Peter Morville
- [ ] A Project Guide to UX Design - Russ Unger

