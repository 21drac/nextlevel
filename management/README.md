# Aprendiendo sobre gestión de proyectos
*******
Es una disciplina que abarca la organización, el planeamiento, la motivación y el control de los recursos con la finalidad de alcanzar los objetivos propuestos para lograr el éxito en uno o varios proyectos dentro de las limitaciones establecidas. Estas limitaciones suelen ser el alcance, el tiempo, la calidad y el presupuesto. La finalidad de esta disciplina es coordinar todos los recursos disponibles para conseguir determinados objetivos; lo cual implica la interacción entre conocimiento, tecnología, entorno, estructuras, procesos, servicios y productos. En este sentido, las soluciones de la gestión y la dirección de proyectos (metodologías, técnicas, tecnologías, modelos, herramientas) deberán dar soporte al proceso de gestión de proyectos. Este proceso a su vez debe ser el acertado, basado en principios razonables.
*******
## Temario | Silabo

1. **Introducción al construir algo**  

2. **Procesos e Integracion**

3. **Alcance y Calidad**

4. **Cronograma y Costos**

5. **Interesados, recursos y comunicaciones** 

6. **Riesgos y adquisiciones**

7. **Gestion del cambio**  
*******
## Bibliografía 
- [Guía de los Fundamentos](https://www.pmi.org/-/media/pmi/documents/public/pdf/pmbok-standards/pmbok-guide-6th-errata.pdf?sc_lang_temp=es-ES)
- [Equipos de proyecto, liderazgo y comunicación](https://www.pmi.org/-/media/pmi/documents/public/pdf/learning/academic-research/project-management-courses/pm-2-project-teams-leadership-and-communication.pdf?v=73c83637-5e76-4a07-b394-432753081abe)
- [Fundamentos de gestión de proyectos en TI / IS / Software
Desarrollo](https://www.pmi.org/-/media/pmi/documents/public/pdf/learning/academic-research/project-management-courses/pm-1-variation-it-pm.pdf?v=8fc29074-2da1-4c5e-8252-503586982f14)
- [Fundamentos de Gestión de Proyectos para Ciencias de la Salud](https://www.pmi.org/-/media/pmi/documents/public/pdf/learning/academic-research/project-management-courses/pm-1-variation-healthcare.pdf?v=754192ab-ec89-4162-908b-1d04db0393e8)
- [Fundamentos de Gestión de Proyectos para Ingenieros](https://www.pmi.org/-/media/pmi/documents/public/pdf/learning/academic-research/project-management-courses/pm-1-variation-engineering.pdf?v=ed6c888a-502f-46e7-8636-c871579be8ba)
- [Fundamentos de la gestión de proyectos](https://www.pmi.org/-/media/pmi/documents/public/pdf/learning/academic-research/project-management-courses/pm-1-foundational.pdf?v=a18f3bb3-f86b-49bd-9c07-1f64ee1b0341)

## Mayor Informacón
Dudas y consultas [Link](https://www.instagram.com/ieeecomsocuch/)
